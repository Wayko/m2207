package tp1;

public class MaBanque {

	public static void main(String[] args) 
	{
		
		//Exercice 2.3
		Compte compte = new Compte(1);
		System.out.println("Le montant du decouvert est de: " + compte.getDecouvert());
		compte.setDecouvert(100);
		System.out.println("Le montant du decouvert est de: " + compte.getDecouvert());
		//Exercice 2.4
		compte.depot(300);
		System.out.println("Le montant du solde est de: " + compte.getSolde());
		//Exercice 2.5
		System.out.println(compte.retrait(500));
		System.out.println(compte.retrait(400));
		
		//Exercice 2.6
		Compte c2 = new Compte(2);
		c2.depot(1000);
		System.out.println("Le montant du solde est de: " + c2.getSolde());
		System.out.println(c2.retrait(600) + "votre solde est actuellement de: " + c2.getSolde());
		System.out.println(c2.retrait(700));
		c2.setDecouvert(500);
		System.out.println(c2.retrait(700) + "votre solde est actuellement de: " + c2.getSolde());
		//Exercice 3.4
		Client client1 = new Client("mathias", "Riviere", c2);
		client1.afficherSolde();
		
	}

}
