package tp1;

public class Client 
{
	
	//Attribut
	private String nomClient, prenomClient;
	private Compte compteCourant;
	
	//Constructeur 
	public Client(String n, String p, Compte compte) 
	{
		
		nomClient = n; 
		prenomClient = p;
		compteCourant = compte;
	}
	
	//Accesseurs 
	public String getN() 
	{
		return nomClient;
	}
	
	public String getP() 
	{
		return prenomClient;
	}
	
	//Methodes
	public double getSolde() 
	{
		return this.compteCourant.getSolde();
	}
	public void afficherSolde() 
	{
		System.out.println("Le solde est de: " + compteCourant.getSolde());
	}
	
}
