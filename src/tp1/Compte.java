package tp1;

public class Compte {
	
	//Attribut 
	 private int numero; 
	 private double solde, decouvert;
	
	//Constructeur 
	public Compte(int numero) 
	{
		this.numero = numero;
		solde = 0; 
		decouvert = 0;
	}
	
	//Accesseurs 
	//Exercice 2.2
	public void setDecouvert (double montant) 
	{
		decouvert = montant;
	}
	
	public double getDecouvert() 
	{
		return decouvert; 
	}
	
	public int getNumero() 
	{
		return numero;
	}
	
	public double getSolde() 
	{
		return solde;
	}
	
	//Methodes
	public void afficherSolde() 
	{
		System.out.println("Votre solde est actuellement de : " + solde);
	}
	//Exercice 2.4
	public void depot(double montant) 
	{
		solde = montant; 
	}
	//Exercice 2.5
	public String retrait(double montant) 
	{
		double retraitMax = decouvert + solde;
		if(montant <= retraitMax)
		{
			solde = solde - montant;
			return "Retrait effectu� ";
		}
		else 
		{
			return "Retrait refus� ";
		}
	}
	
}
