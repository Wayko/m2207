package td1;

public class Exercice1
{

	public static void main(String[] args)
	{
		//Attribut
		String nom = "Bob";
		float poids = 58;
		int age = 18, retraite = 65, age_Vieilli = 40, bobAge = pdm(age, poids);
		
		double taille = 1.82, poidsVieilli = p_40(age_Vieilli), IMC = imc(poidsVieilli, taille);
		
		//Afficher son age, son poids et le nombre d'annee avant sa retraite. 
		System.out.println(nom + " a " + age + " ans, p�se "+ poids+" kgs"
				+ " et il lui reste "+ (retraite) +" ans avant d'�tre � la retraite.");
		//Afficher l'age auquels il depassera 70kg.
		System.out.println(nom + " depassera les 70 kgs a " + bobAge + " ans.");
		//Afficher le poids qu'il aura a 40 ans. 
		System.out.println("A "+age_Vieilli+" ans, " + nom +" pesera " + poidsVieilli + " kgs.");
		//Afficher son indice de masse corporelle quand il aura 40 ans.
		System.out.println("A "+age_Vieilli+" ans, " + nom + " aura un IMC de " + IMC);
		
	}
	
	// Methode permettant de calculer l'age ou il pesera plus de 70kg.
	public static int pdm(int b_age, float b_poids)
	{
		while ( b_poids < 70 )
		{
			b_poids+=1.2;
			b_age++;
		}
		return b_age;
	}
	
	// Methode permettant de calculer le poids de Bob a 40 ans.
	public static double p_40(int ageChoisi)
	{
		double poidsBob = 58 + 1.2 * (ageChoisi - 18);
		return poidsBob;
	}
	
	// Methode permettant de calculer l'indice de masse corporelle.
	public static double imc(double bob_poids, double bob_taille)
	{
		double indice_MC = bob_poids / (bob_taille * bob_taille);
		return indice_MC;
	}
	
	
}
