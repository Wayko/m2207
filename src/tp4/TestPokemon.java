package tp4;

public class TestPokemon {

	public static void main(String[] args) {

		//Exercice 1.4
		Pokemon pikachu = new Pokemon("pikachu"); 
		pikachu.sePresenter();
		
		//Exercice 1.5
		pikachu.manger();
		pikachu.sePresenter();
		
		//Exercice 1.6
		pikachu.vivre();
		pikachu.sePresenter();
		
		//Exercice 1.8
		int cycle = 0;
		while(pikachu.isAlive()) 
		{
			pikachu.manger();
			pikachu.vivre();
			cycle++;
		}
		System.out.println(pikachu.getNom() + " a vecu " + cycle + " cycles !");

		//pikachu a vecu 4 cycles !
		//pikachu a vecu 5 cycles !
		//pikachu a vecu  5 cycles !

	}

}
