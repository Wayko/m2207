package tp4;

public class Pokemon {

	//Attributs
	private int energie, maxEnergie, puissance;
	private String nom; 
	
	//Constructeurs 
	public Pokemon(String nom) 
	{
		this.nom = nom; 
		this.maxEnergie = 50 + (int) (Math.random() * ((90 - 50 ) + 1));	
		this.energie =  30 + (int) (Math.random() * ((maxEnergie - 30) + 1));
		this.puissance = 3 + (int)(Math.random() * ((10 - 3) + 1));

	}
	
	//Accesseurs 
	public String getNom() //Accesseurs permettant de recuperer le nom
	{
		return this.nom;
	}
	
	public int getEnergie() //Accesseurs permettant de recuperer la valeur de l'energie
	{
		return this.energie;
	}
	
	public int getPuissance() 
	{
		return this.puissance;
	}
	
	//Methodes 
	public void sePresenter() //Methode permettant de se presenter 
	{
		System.out.println("Je suis " + nom + ", j'ai " + energie + " points d'energie " + "(" + maxEnergie + " max) et une puissance de " + puissance);
	}
	
	public void manger() 
	{
		if (this.energie > 0) 
		{
			int nourriture = 10 + (int)(Math.random() * ((30 - 10) + 1));
			
			if ((this.energie + nourriture) <= this.maxEnergie) 
			{
				this.energie += nourriture;
			}
		}
	}
	
	public void vivre() 
	{
		this.energie -= (20 + (int)(Math.random() * ((40 - 20) + 1)));
		if (this.energie < 0) 
		{
			this.energie = 0;
		}
	}
	
	public boolean isAlive() 
	{
		if (this.energie <= 0) 
		{
			this.energie = 0;
			return false;
		} 
		else 
		{
			return true;
		}
	}
	
	public void perdreEnergie(int perte) 
	{
		this.energie -= perte;
	}

	public void attaquer(Pokemon adversaire) 
	{
		perdreEnergie()
	}


}
