package tp2;

public class Forme 
{
	
	//Attributs 
	private String couleur; 
	private boolean coloriage;
	private static int compteur;

	
	//Constructeurs
	public Forme() 
	{
		couleur = "orange";
		coloriage = true; 
		compteur++;
	}
	
	public Forme(String c, boolean r) 
	{
		couleur = c;
		coloriage = r;
		compteur++;
	}
	
	//Accesseurs
	public String getCouleur() 
	{
		return couleur;
	}
	
	public void setCouleur(String c) 
	{
		couleur = c;
	}
	
	public boolean isColoriage() 
	{
		return coloriage;
	}
	
	public void setColoriage(boolean b) 
	{
		coloriage = b;
	}
	
	//Methodes
	public String seDecrire() 
	{	
		return "une Forme de couleur " + couleur + " et de coloriage " + coloriage;
	}
	
	public static String nombreForme() 
	{
		return "Il y a actuellement " + compteur + " forme(s)";
	}
}
