package tp2;

public class Cylindre extends Cercle{
	
	//Attributs 
	private double hauteur; 
	
	//Constructeurs 
	public Cylindre() 
	{
		super();
		hauteur = 1.0;
	}
	
	public Cylindre(double r, String couleur, boolean coloriage, double h) 
	{
		super(r,couleur,coloriage);
		hauteur = h;
	}
	
	//Accesseurs 
	public double getHauteur() 
	{
		return hauteur;
	}
	
	public void setHauteur(double h) 
	{
		hauteur = h;
	}
	
	//Methodes
	public String seDecrire() 
	{
		return "Un Cylindre de hauteur " + hauteur + " est issue d'" + super.seDecrire();
	}
	
	public double calculerVolume() 
	{
		return super.calculerPerimetre() * hauteur;
	}
}
