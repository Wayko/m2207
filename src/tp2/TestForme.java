package tp2;

public class TestForme 
{

	public static void main(String[] args) 
	{

		Forme f1 = new Forme();
		Forme f2 = new Forme("vert", false);

		//Exercice 1.4
		//On affiche les valeurs de f1 et f2 obtenues avec les accesseurs
		System.out.println("f1 : " + f1.getCouleur() + " - " + f1.isColoriage());
		System.out.println("f2 : " + f2.getCouleur() + " - " + f2.isColoriage());
		
		//Exercice 1.5
		f1.setCouleur("rouge");
		//On affiche la valeur de f1 apr�s la modification de la couleur orange en rouge
		System.out.println("f1 : " + f1.getCouleur() + " - " + f1.isColoriage());
		
		//Exercice 1.7
		System.out.println(f1.seDecrire());
		System.out.println(f2.seDecrire());
		
		//Exercice 2.4
		Cercle c1 = new Cercle();
		
		//Exercice 2.5
		System.out.println("Un Cercle de rayon " + c1.getRayon() + " est issue d'une Forme de couleur " + c1.getCouleur() + " et de coloriage " + c1.isColoriage());
		
		//Exercice 2.6
		System.out.println(c1.seDecrire());
		
		//Exercice 2.8
		Cercle c2 = new Cercle(2.5);
		System.out.println(c2.seDecrire());
		
		//Exercice 2.10
		Cercle c3 = new Cercle(3.2, "jaune", false);
		System.out.println(c3.seDecrire());
		
		//Exercice 2.11
		//On affiche l'air et le perimetre du cercle c2 et c3
		System.out.println("L'air du cercle c3 est de: " + c2.calculerAire() + " et son perimetre vaut: " + c2.calculerPerimetre());
		System.out.println("L'air du cercle c3 est de: " + c3.calculerAire() + " et son perimetre vaut: " + c3.calculerPerimetre());
		
		//Exercice 3.2
		Cylindre cy1 = new Cylindre();
		//On affiche les informations du cylindre grace a la methode seDrecrire herite de la classe Cercle 
		System.out.println(cy1.seDecrire());
		
		//Exercice 3.4 
		Cylindre cy2 = new Cylindre(1.3,"bleu", true, 4.2);
		System.out.println(cy2.seDecrire());
		
		//Exercice 3.5
		//On affiche l'air du cylindre cy1 et cy2
		System.out.println("L'air du cylindre cy1 est: " + cy1.calculerVolume());
		System.out.println("L'air du cylindre cy2 est: " + cy2.calculerVolume());
		
		//Exercice 4.1
		System.out.println(f1.nombreForme());
	}

}
