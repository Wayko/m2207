package tp2;

public class Cercle extends Forme
{

	//Attributs 
	private double rayon; 
	
	//Constructeurs 
	public Cercle() 
	{
		super();
		rayon = 1.0;
	}
	
	public Cercle(double r) 
	{
		super();
		rayon = r;
	}
	
	public Cercle(double r, String couleur, boolean coloriage) 
	{
		super(couleur,coloriage);
		rayon = r;
	}
	
	//Accesseurs
	public double getRayon() 
	{
		return rayon;
	}
	
	public void setRayon(double r) 
	{
		rayon = r;
	}
	
	//Methodes
	
	public String seDecrire() 
	{
		return "un Cercle de rayon " + rayon + " est issue d'" + super.seDecrire();
	}
	
	public double calculerAire() 
	{
		double p = 2 * Math.PI * rayon;
		return p;
	}
	
	public double calculerPerimetre() 
	{
		double p = Math.PI * (rayon * rayon);
		return p;
	}
	

}
