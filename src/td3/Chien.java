package td3;

public class Chien extends Animal {
	
	public String affiche() {
		return "Je suis un chien ";
	}
	
	public String cri() {
		return "Ouaf Ouaf";
	}
	
	/*public String origine() {
		return "Ouaf Ouaf";
	}*/
}
