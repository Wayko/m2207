package td3;

public class TestAnimal 
{

	public static void main(String[] args) 
	{
		//Attribut 
		Animal animal = new Animal(); 
		//Exercice 1.2
		System.out.println(animal.toString());
		//Exercice 1.3, 1.4 et 1.5
		System.out.println(animal.affiche() + animal.cri());
		//Exercice 2.1
		Chien rex = new Chien(); 
		//Exercice 2.2, 2.3, 2.4 et 2.5
		System.out.println(rex.affiche() + rex.cri());
		
		//Exercice 2.7
		System.out.println(animal.origine());
		System.out.println(rex.origine());
		
		//Exercice 3.1, 3.2, 3.3
		Chat minou = new Chat(); 
		System.out.println(minou.affiche() + minou.miauler());
	}

}
