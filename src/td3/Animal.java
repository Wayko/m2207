package td3;

public class Animal {
	
	//Constructeur 
	public String affiche() {
		return "Je suis un animal";
	}
	
	public String cri() {
		return "...";
	}
	
	public final String origine() {
		return "La classe Animal est la m�res de toutes les classes";
	}
}
