package tp6;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket; 

public class MonServeur 
{
	public static void main(String[] args) 
	{ 
		ServerSocket monServerSocket;
		Socket monSocketClient;
		BufferedReader monBufferedReader;
		try 
		{
			monServerSocket = new ServerSocket(8888); 
			System.out.println("ServerSocket: " + monServerSocket); 
			
			monSocketClient = monServerSocket.accept();
			System.out.println("Le client s'est connecté"); 
			
			monBufferedReader = new BufferedReader(new InputStreamReader(monSocketClient.getInputStream())); 
			String message = monBufferedReader.readLine();
			System.out.println("Message : " + message);
			
			monServerSocket.close();
			monSocketClient.close();
			

		} 
		catch (Exception e) 
		{ 
			e.printStackTrace();
		} 

	}
}