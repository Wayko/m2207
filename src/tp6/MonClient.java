package tp6;
import java.io.PrintWriter;
import java.net.Socket; 

public class MonClient
{
	public static void main(String[] args) 
	{ 
		

		try 
		{
			Socket monSocket;
			monSocket = new Socket("localhost", 8888); 
			System.out.println("Client: " + monSocket); 
			PrintWriter monPrintWriter;
			monPrintWriter = new PrintWriter(monSocket.getOutputStream()); 
			System.out.println("Envoi du message : Hello World"); 
			monPrintWriter.println("Hello World");
			monPrintWriter.flush();
			monSocket.close();
			


		} 
		catch (Exception e) 
		{ 
			e.printStackTrace();
		} 

	}
}