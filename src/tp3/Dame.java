package tp3;

public class Dame extends Humain
{
	
	//Attributs
	private boolean libre = true;
	
	//Constructeurs
	public Dame(String nom) 
	{	
		super(nom);
		this.libre = true;
		boisson = "Martini";
	}
	
	//Accesseurs
	
	public String quelEstTonNom() 
	{
		
		return "Miss " + this.nom;
	}
	
	//Methodes
	 public void priseEnOtage() 
	 {
		 
		 this.libre = false;
		 parler("Au secours !");
		 
	 }
	 
	 public void estLiberee() 
	 {
		 
		 this.libre = true;
		 parler("Merci Cowboy");
		 
	 }
	 
	 public void sePresenter() 
	 {
		 String p1;
					if (libre == true) { p1 = "libre"; }
					else { p1 = "kidnappe"; }
				
			super.sePresenter();
			parler("Actuellement, je suis " + p1);
			
	 }

}
