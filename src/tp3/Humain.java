package tp3;

public class Humain 
{
		//Attributs
		protected String nom, boisson;
		
		//Constructeurs
		public Humain(String nom)
		{
			this.nom = nom; 
			this.boisson = "lait";		
		}
		
		public Humain(String nom, String boisson) 
		{
			this.nom = nom;
			this.boisson = boisson;
		}

		
		//Methodes
		public String quelEstTonNom() 
		{
			return nom;
		}
		
		public String quelEstTaBoisson() 
		{
			return boisson;
		}
		
		public void parler(String texte) 
		{
			System.out.println("("+ nom + ")" + " - " + texte);
		}
		
		public void sePresenter() 
		{
			parler("Bonjour, je suis " + quelEstTonNom() + " et ma boisson pr�f�r�e est le " + quelEstTaBoisson());
		}
		
		public void boire() 
		{
			parler("Ah ! un bon verre de " + quelEstTaBoisson() + " ! GLOUPS !");
		}
}
