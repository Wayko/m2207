package tp3;

public class Brigand extends Humain{

		//Attribut
		private String look;
		private int nKidnap, reward;
		private boolean prison;
	
		//Constructeurs
		//Exercice 3.2
		public Brigand(String nom) 
		{
			super(nom, "cognac");
			this.look = "mechant";
			this.prison = false;
			this.reward = 100;
			this.nKidnap = 0;
		}
		
		//Methodes
		
		public int getRecompense() 
		{
			return reward;
		}
		
		public String quelEstTonNom() 
		{ 
			 return (this.nom + " le " + this.look);
		}
		
		public void sePresenter() 
		{ 
			super.sePresenter();
			super.parler("J'ai l'aire Mechant et j'ai enlev� " + this.nKidnap + " dames.");
			super.parler("Ma t�te est mise � prix " + this.reward + " $ !!");
		}

		public void enleve(Dame dame) 
		{
			super.parler("Ah ah ! " + dame.quelEstTonNom() + ", tu es ma prisonni�re !");
			dame.priseEnOtage(); // Appel de la methode priseEnOtage qui changera le status de libert� de la dame
			this.nKidnap++; // Incrementation du nombre de dame captur� et sa valeur
			this.reward += 100;
		}
		
		public void emprisonner(Sherif s) 
		{ 
			super.parler("Damned, je suis fait ! " + s.quelEstTonNom() + ", tu m�as eu !");
			this.prison = true;
		}

}
