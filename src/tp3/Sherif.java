package tp3;

public class Sherif extends Cowboy{

	//Attribut
	
	private int nBrigand;
	
	//Constructeurs
	
	public Sherif(String nom) 
	{ 
		super(nom);
		nBrigand = 0;
	}
	
	public Sherif(String nom, String boisson) 
	{
		super(nom, boisson);
		nBrigand = 0;
	}
	
	
	// Methodes
	
	public void sePresenter() 
	{ 
		super.sePresenter();
		super.parler("J'ai d�ja captur� " + this.nBrigand + " brigand(s) depuis le debut de ma carri�re.");
	}
	
	public void coffrer(Brigand b) 
	{ 
		super.parler("Au nom de la loi, je vous arr�te, " + b.quelEstTonNom() + " !");
		b.emprisonner(this); // Change le statut de "prison" du brigand
		nBrigand++; // Incrementation du nombre de brigand arr�t� de 1
	}

}
