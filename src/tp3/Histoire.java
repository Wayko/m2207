package tp3;

public class Histoire {

	public static void main(String[] args) {
		
		//Exercice 1.7 (On affiche l'humain qu'on a cr�er on lui demande de se pr�senter et ensuite de boire)
		Humain humain = new Humain("bob");
		humain.sePresenter();
		humain.boire();
		
		//Exercice 2.4 
		Dame dame = new Dame("Fiona");
		dame.sePresenter();
		dame.priseEnOtage();
		dame.estLiberee();

		//Exercice 3.4
		//On cr�er un brigand de type Brigand et on lui demande de se presenter
		Brigand brigand = new Brigand("Gunther");
		brigand.sePresenter();
		
		//Exercice 4.3
		//On cr�er un cowboy de type Cowboy et on lui demande de se presenter
		Cowboy cowboy = new Cowboy("Simon");
		cowboy.sePresenter();
		
		//Exercice 7.2
		brigand.enleve(dame);
		brigand.sePresenter();
		dame.sePresenter();
		
		//Exercice 7.6
		cowboy.tire(brigand);
		cowboy.libere(dame);
		dame.sePresenter();
		
		//Exercice 8.4
		Sherif sherif = new Sherif("Luky-luck");
		sherif.sePresenter();
		sherif.coffrer(brigand);
		sherif.sePresenter();

		//Exercice 9
		
		Cowboy clint = new Sherif("Clint");
		//On ne peut demander a ce cowboy de coffrer car la methode appartient a sherif 
		
	}

}

