package tp3;

public class Cowboy extends Humain{

	//Attribut 
	
	private int popularite;
	private String caracteristique;
	
	//Constructeur
	
	public Cowboy(String nom) 
	{
		super(nom, "whiskey"); 
		this.popularite = 0;
		this.caracteristique = "vaillant";
	}
	
	//Methodes 
	
	public void tire(Brigand brigand) 
	{
		System.out.println("Le " + this.caracteristique + " " + this.nom + " tire sur " + brigand.nom + " ! PAN !");
		super.parler("Prend �a, voyou !"); 

	}
	
	public void libere(Dame dame) 
	{
		dame.estLiberee(); //Appel de ma methode permettant de liberer la dame
		this.popularite += 10;
	}
}
