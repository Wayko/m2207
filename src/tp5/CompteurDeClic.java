package tp5;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.event.*;

public class CompteurDeClic extends JFrame  implements ActionListener
{
	//Attributs
	private JButton b;
	private Container panneau;
	private JLabel monLabel;
	private int i = 0;

	
	//Constructeurs 
	public CompteurDeClic ()
	{
		super(); 
		this.setTitle("Compteur de clic");
		this.setSize(400,200);
		this.setLocation(20,20); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		b = new JButton("Click!");
		monLabel = new JLabel("Vous avez cliqué 0 fois");
		
		Container panneau = getContentPane(); 
		
		panneau.setLayout(new GridLayout());
		panneau.add(b);
		panneau.add(monLabel);
		
		b.addActionListener(this);
		
		this.setVisible(true);
		
	
	}

	//Methodes
	
	public void actionPerformed(ActionEvent e)
	{
		System.out.println("Une action a été détectée");
		i++;
		monLabel.setText("Vous avez cliqué" + i + "fois");	
		
	}

	
	//Methode main 

	public static void main(String[] args) 
	{
		CompteurDeClic app = new CompteurDeClic (); 

		
	}	

}