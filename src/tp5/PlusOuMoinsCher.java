package tp5;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.event.*;

public class PlusOuMoinsCher extends JFrame  implements ActionListener
{
	//Attributs
	private JButton b;
	private Container panneau;
	private JLabel choix, reponse;
	private JTextField proposition;
	private int i = 0;

	
	//Constructeurs 
	public PlusOuMoinsCher ()
	{
		super(); 
		this.setTitle("Plus ou moins cher");
		this.setSize(350,150);
		this.setLocation(300,150); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		b = new JButton("Click!");
		reponse = new JLabel("La réponse");
		choix = new JLabel("Votre proposition:");
		proposition = new JTextField("Tapez ici!");
		
		Container panneau = getContentPane(); 
		
		panneau.setLayout(new GridLayout(2,2));
		panneau.add(choix);
		panneau.add(monLabel);
		
		b.addActionListener(this);
		
		this.setVisible(true);
		
	
	}

	//Methodes
	
	public void actionPerformed(ActionEvent e)
	{
		System.out.println("Une action a été détectée");
		i++;
		monLabel.setText("Vous avez cliqué" + i + "fois");	
		
	}

	
	//Methode main 

	public static void main(String[] args) 
	{
		PlusOuMoinsCher app = new PlusOuMoinsCher (); 

		
	}	

}