package tp5;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class MonAppliGraphique extends JFrame 
{
	//Attributs
	private JButton b0, b1, b2, b3, b4;
	private Container panneau;
	private JLabel monLabel;
	private JTextField monTextField;
	
	//Constructeurs 
	public MonAppliGraphique ()
	{
		super(); 
		this.setTitle("Ma première application");
		this.setSize(400,200);
		this.setLocation(20,20); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		b0 = new JButton("Boutton 0");
		b1 = new JButton("Boutton 1");
		b2 = new JButton("Boutton 2");
		b3 = new JButton("Boutton 3");
		b4 = new JButton("Boutton 4");
		monLabel = new JLabel("Je suis un JLabel");
		monTextField = new JTextField("Taper ici le texte à saisir");
		
		Container panneau = getContentPane(); 
		
		panneau.setLayout(new GridLayout(3,2));
		panneau.add(b0, BorderLayout.CENTER); 
		panneau.add(b1,BorderLayout.NORTH);
		panneau.add(b2,BorderLayout.EAST);
		panneau.add(b3,BorderLayout.SOUTH);
		panneau.add(b4,BorderLayout.WEST);
		panneau.add(monLabel);
		panneau.add(monTextField);

		
		this.setVisible(true);
		
	
	}

	//Methodes

	
	//Methode main 

	public static void main(String[] args) 
	{
		MonAppliGraphique app = new MonAppliGraphique (); 

		
	}	

}
